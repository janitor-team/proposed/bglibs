/* This file was automatically generated from bg-installer.cli, do not edit. */
#include <string.h>
#include <bglibs/obuf.h>
#include <bglibs/cli.h>
const char program[] = "bg-installer";
const char cli_args_usage[] = "[top-directory] <LIST";
const int cli_args_min = 0;
const int cli_args_max = 1;
const int msg_show_pid = 0;
int msg_debug_bits = 0;
void cli_show_help(void) {
  obuf_puts(&outbuf,
"Standardized installer program.\n"
"\n"
"  -v, --verbose  List the files as they are processed.\n"
"  -n, --dry-run  Don't modify any files, just display.\n"
"  -c, --check    Check destinations instead of installing.\n"
"\n"
"  -h, --help     Display this help and exit\n"
"See http://untroubled.org/bglibs/docs/group__installer.html\n"
"for more information\n"
"");
}
int opt_check = 0;
int opt_dryrun = 0;
int opt_verbose = 0;
cli_option cli_options[] = {
  { 'v', "verbose", CLI_FLAG, 1, &opt_verbose, 0, 0 },
  { 'n', "dry-run", CLI_FLAG, 1, &opt_dryrun, 0, 0 },
  { 'c', "check", CLI_FLAG, 1, &opt_check, 0, 0 },
  {0,0,0,0,0,0,0}
};
