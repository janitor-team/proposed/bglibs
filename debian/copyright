Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bglibs
Upstream-Contact: Bruce Guenter <bruce@untroubled.org>
Files-Excluded: doc/html doc/latex
Source: http://untroubled.org/bglibs/

Files: *
Copyright: 2002-2015 Bruce Guenter <bruce@untroubled.org>
License: LGPL-2.1+

Files: debian/*
Copyright: 2002 Gerrit Pape <pape@smarden.org>
           2017 Dmitry Bogatov <KAction@gnu.org>
License: LGPL-2.1+

Files: crypto/md5-crypt.c
       crypto/md5.c
       include/md5.h
Copyright: 1996-1997 Free Software Foundation, Inc.
License: LGPL-2+

Files: crypto/surfrand.c
       pwcmp/client.c
       pwcmp/hex_encode.c
       pwcmp/module.c
Copyright: 2001 Bruce Guenter <bruce@untroubled.org>
License: GPL-2+

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 The full text can be found at /usr/share/common-licenses/LGPL-2.1.

License: LGPL-2+
 The GNU C Library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 The GNU C Library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 The full text can be found at /usr/share/common-licenses/LGPL-2.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 The full text can be found at /usr/share/common-licenses/GPL-2.
